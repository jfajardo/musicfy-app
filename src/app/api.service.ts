import {Injectable} from '@angular/core';
import {HttpHeaders} from '@angular/common/http';
import {HandleError, HttpErrorHandler} from './http-error-handler.service';
import {HttpClient} from '../../node_modules/@angular/common/http';
import {Observable} from 'rxjs';
import {Globals} from './globals';
import {catchError} from 'rxjs/operators';
import {HttpParams} from '@angular/common/http';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Token ${localStorage.getItem('auth_token')}`
    })
};

@Injectable({
    providedIn: 'root'
})
export class ApiService {
    handleError: HandleError;

    constructor(
        private http: HttpClient,
        httpErrorHandler: HttpErrorHandler
    ) {
        this.handleError = httpErrorHandler.createHandleError('ApiService');
    }

    listAlbums(artist: string): Observable<any[]> {
        artist = artist.trim();

        const options = artist ?
            {params: new HttpParams().set('artist', artist)} : {};

        return this.http.get<any[]>(`${Globals.API_URL}albums`, options)
            .pipe(
                catchError(this.handleError<any[]>('searchAlbums', []))
            );
    }

    listArtists(artist: string): Observable<any[]> {

        const options = artist ?
            {params: new HttpParams().set('artist', artist)} : {};

        return this.http.get<any[]>(`${Globals.API_URL}artists`, options)
            .pipe(
                catchError(this.handleError<any[]>('searchArtists', []))
            );
    }

    listAlbumTracks(album_id: string): Observable<any[]> {

        return this.http.get<any[]>(`${Globals.API_URL}album/${album_id}`, httpOptions)
            .pipe(
                catchError(this.handleError<any[]>('listAlbumTracks', []))
            );
    }

    favoriteTrack (data: any): Observable<any> {
        return this.http.post<any>(`${Globals.API_URL}favorite-track`, data, httpOptions)
            .pipe(
                catchError(this.handleError('loginAuth', {auth_token: null}))
            );
    }

    favorites(): Observable<any[]> {
        return this.http.get<any[]>(`${Globals.API_URL}favorites`, httpOptions)
            .pipe(
                catchError(this.handleError<any[]>('listFavorites', []))
            );
    }
}
