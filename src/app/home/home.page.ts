import {Component} from '@angular/core';
import {ApiService} from '../api.service';
import {NavigationExtras, Router} from '@angular/router';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage {
    albums: any[];
    artists: any[];
    navExtras: NavigationExtras;
    tab_active: any = 'albums_tab';
    constructor(
        public api: ApiService,
        private router: Router
        ) {}
    ionViewWillEnter(): void {
        this.api.listAlbums('').subscribe(response => {
            this.albums = response;
        });
        this.api.listArtists(null).subscribe(response => {
            this.artists = response;
        });
    }
    artistsTab() {
        this.tab_active = 'artists_tab';
    }
    albumsTab() {
        this.tab_active = 'albums_tab';
    }
    goToAlbumsArtist(artist) {
        this.navExtras = {
            queryParams: artist
        };
        this.router.navigate(['albums'], this.navExtras);
    }
    goToAlbumTracks(album) {
        this.navExtras = {
            queryParams: {data: JSON.stringify(album)}
        };
        this.router.navigate(['album'], this.navExtras);
    }
}
