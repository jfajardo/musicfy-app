import {Component, OnInit} from '@angular/core';
import {NavController} from '@ionic/angular';
import {AuthService} from '../auth.service';

@Component({
    selector: 'app-about',
    templateUrl: './about.page.html',
    styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {

    constructor(
        public navCtrl: NavController,
        public authService: AuthService,
        ) {
    }

    ngOnInit() {
    }

    exit() {
        this.authService.logout().subscribe(res => {
            localStorage.clear();
           this.navCtrl.goRoot('login');
        });
    }
}
