import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class AudioService {
    track = new Audio();
    constructor() {
    }

    play(track: any) {
        this.track.src = track.song;
        this.track.load();
        this.track.play();
    }
    pause() {
        if (this.track.paused) {
            this.track.play();
        } else {
            this.track.pause();
        }
    }
    is_paused() {
        return this.track.paused;
    }
}
