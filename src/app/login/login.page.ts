import { Component, OnInit } from '@angular/core';
import {FormGroup, Validators} from '@angular/forms';
import {FormBuilder} from '@angular/forms';
import {AuthService} from '../auth.service';
import {NavController, ToastController} from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {
    loginForm: FormGroup;
    constructor(
        public formBuilder: FormBuilder,
        public authService: AuthService,
        public toastController: ToastController,
        public navCtrl: NavController
        ) {
        this.loginForm  = this.makeForm();
    }

    private makeForm() {
        return this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
    }

    ngOnInit(): void {}

    submitForm() {
        this.authService.login(this.loginForm.value).subscribe(response => {
           if (response.auth_token) {
               localStorage.setItem('auth_token', response.auth_token);
               this.navCtrl.goRoot('');
           } else {
               this.showToast('Usuario y/o Contraseña incorrecta!');
           }
        });
    }

    async showToast(message: string) {
        const toast = await this.toastController.create({
            message: message,
            duration: 2000,
            position: 'top',
        });
        toast.present();
    }

}
