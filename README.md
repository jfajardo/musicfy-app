# Musicfy App

Application developed with Ionic 4 that consumes written APIs from Django Rest Framework

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.


### Installing

You need Ionic and npm

```
npm install
```

## Running the tests

To run the project

```
ionic serve --lab
```

## Built With

* [IONIC](https://ionicframework.com/) - The web and mobile framework used

## Authors

* **Jonhatan Fajardo** - - [My Works](https://jfajardo.github.com)
