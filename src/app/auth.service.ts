import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {HandleError, HttpErrorHandler} from './http-error-handler.service';
import {Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Globals} from './globals';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
    })
};

const httpOptionsAuth = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Token ${localStorage.getItem('auth_token')}`
    })
};
@Injectable({
    providedIn: 'root'
})
export class AuthService {
    handleError: HandleError;
    constructor(
        private http: HttpClient,
        httpErrorHandler: HttpErrorHandler
    ) {
        this.handleError = httpErrorHandler.createHandleError('LoginService');
    }

    login (data: any): Observable<any> {
        return this.http.post<any>(`${Globals.API_URL}authtoken/login`, data, httpOptions)
            .pipe(
                catchError(this.handleError('loginAuth', {auth_token: null}))
            );
    }

    logout (): Observable<any> {
        return this.http.post<any>(`${Globals.API_URL}authtoken/logout`, httpOptionsAuth)
            .pipe(
                catchError(this.handleError('loginAuth', {auth_token: null}))
            );
    }
}
