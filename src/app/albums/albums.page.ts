import {ActivatedRoute, NavigationExtras, Router} from '@angular/router';
import {Component, OnInit} from '@angular/core';
import {ApiService} from '../api.service';

@Component({
    selector: 'app-albums',
    templateUrl: './albums.page.html',
    styleUrls: ['./albums.page.scss'],
})
export class AlbumsPage implements OnInit {
    artist: any;
    albums: any[];
    navExtras: NavigationExtras;
    constructor(
        private route: ActivatedRoute,
        public api: ApiService,
        private router: Router
        ) {
        this.route.queryParams.subscribe(params => {
           this.artist = params;
            this.api.listAlbums(this.artist.id).subscribe(response => {
                this.albums = response;
            });
        });
    }

    ngOnInit() {
    }

    goToAlbumTracks(album) {
        this.navExtras = {
            queryParams: {data: JSON.stringify(album)}
        };
        this.router.navigate(['album'], this.navExtras);
    }

}
