import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ApiService} from '../api.service';
import {AudioService} from '../audio.service';
import {ToastController} from '@ionic/angular';

@Component({
    selector: 'app-album',
    templateUrl: './album.page.html',
    styleUrls: ['./album.page.scss'],
})
export class AlbumPage implements OnInit {
    data: any;
    tracks: any[];
    playing_id: any;
    paused_id: any;
    is_playing: any;

    constructor(
        private route: ActivatedRoute,
        public api: ApiService,
        public audio: AudioService,
        public toastController: ToastController,
        ) {
        this.route.queryParams.subscribe(params => {
            this.data = JSON.parse(params.data);
            this.api.listAlbumTracks(this.data.id).subscribe(response => {
                this.tracks = response['tracks'];
            });
        });
    }

    ngOnInit() {
    }
    playSong(track) {
        this.playing_id = track.id;
        this.paused_id = track.id;
        this.audio.play(track);
        this.is_playing = this.audio.is_paused();
        this.showToast(`🎵 Estás escuchando ${track.name}`);
    }
    pauseSong(track) {
        this.paused_id = track.id;
        this.audio.pause();
        this.is_playing = this.audio.is_paused();
        if (!this.is_playing) {
            this.showToast(`▶️ Continúa escuchando ${track.name}`);
        } else {
            this.showToast(`⏸ Has pausado ${track.name}`);
        }
    }
    addFavorite(track) {
        this.api.favoriteTrack({track: track.id}).subscribe(res => {
            track.is_favorite = res.is_favorite;
            if (res.is_favorite) {
                this.showToast(`❤️ Has agregado ${track.name} a tu lista de favoritas`);
            } else {
                this.showToast(`😢️ Has quitado ${track.name} a tu lista de favoritas`);
            }
        });
    }
    async showToast(message: string) {
        const toast = await this.toastController.create({
            message: message,
            duration: 2000,
            position: 'top',
        });
        toast.present();
    }
}
