import {Component, OnInit} from '@angular/core';
import {ApiService} from '../api.service';
import {AudioService} from '../audio.service';
import {ToastController} from '@ionic/angular';

@Component({
    selector: 'app-favorites',
    templateUrl: './favorites.page.html',
    styleUrls: ['./favorites.page.scss'],
})
export class FavoritesPage{
    favorites: any[];
    playing_id: any;
    paused_id: any;
    is_playing: any;

    constructor(
        public api: ApiService,
        public audio: AudioService,
        public toastController: ToastController,
    ) {
    }

    ionViewWillEnter(): void {
        this.loadData();
    }
    doRefresh(refresher) {
        this.loadData();
        refresher.target.complete();
    }
    loadData() {
        this.api.favorites().subscribe(response => {
            this.favorites = response;
        });
    }
    playSong(track) {
        this.playing_id = track.id;
        this.paused_id = track.id;
        this.audio.play(track);
        this.is_playing = this.audio.is_paused();
        this.showToast(`🎵 Estás escuchando ${track.name}`);
    }
    pauseSong(track) {
        this.paused_id = track.id;
        this.audio.pause();
        this.is_playing = this.audio.is_paused();
        if (!this.is_playing) {
            this.showToast(`▶️ Continúa escuchando ${track.name}`);
        } else {
            this.showToast(`⏸ Has pausado ${track.name}`);
        }
    }
    addFavorite(track) {
        this.api.favoriteTrack({track: track.id}).subscribe(res => {
            this.favorites = this.favorites.filter(favorite => { return favorite.track.id !== track.id; });
            this.showToast(`😢️ Has quitado ${track.name} a tu lista de favoritas`);
        });
    }
    async showToast(message: string) {
        const toast = await this.toastController.create({
            message: message,
            duration: 2000,
            position: 'top',
        });
        toast.present();
    }
}
