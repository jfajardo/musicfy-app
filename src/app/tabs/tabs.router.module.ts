import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {TabsPage} from './tabs.page';
import {HomePage} from '../home/home.page';
import {FavoritesPage} from '../favorites/favorites.page';
import {AboutPage} from '../about/about.page';

const routes: Routes = [
    {
        path: 'tabs',
        component: TabsPage,
        children: [
            {
                path: 'home',
                outlet: 'home',
                component: HomePage
            },
            {
                path: 'favorites',
                outlet: 'favorites',
                component: FavoritesPage
            },
            {
                path: 'about',
                outlet: 'about',
                component: AboutPage
            },
        ]
    },
    {
        path: '',
        redirectTo: '/tabs/(home:home)',
        pathMatch: 'full'
    },
    {
        path: '',
        redirectTo: '/tabs/(favorites:favorites)',
        pathMatch: 'full'
    },
    {
        path: '',
        redirectTo: '/tabs/(about:about)',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TabsPageRoutingModule {
}
